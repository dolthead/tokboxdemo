package com.example.tokbox;

import com.opentok.*;
import com.opentok.exception.OpenTokException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by I68133 on 1/27/2016.
 */
@Controller
@EnableAutoConfiguration
public class Api {
    private static final int API_KEY = 45477652; // YOUR API KEY
    private static final String API_SECRET = "a61ea909c9ace09ff7f0fa642ba2a247d56e8f5b";
    private static OpenTok opentok = new OpenTok(API_KEY, API_SECRET);
    private static Session session = null;

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.GET, value = "/session")
    @ResponseBody
    public Map<String, String> getToken() throws OpenTokException {
        Map<String, String> returnMap = new HashMap<String, String>();
        returnMap.put("token", session.generateToken());
        returnMap.put("sessionId", session.getSessionId());
        returnMap.put("apiKey", Integer.toString(API_KEY));
        return returnMap;
    }

    public static void main(String[] args) throws OpenTokException {
        session = opentok.createSession(new SessionProperties.Builder()
                .mediaMode(MediaMode.ROUTED)
                .build());
        SpringApplication.run(Api.class, args);
    }
}
